/**
 * Realiza uma requisição ajax
 *
 * @param url
 * @param dataType
 * @param type
 * @param params
 * @param callbackAjax
 */
function ajaxRequest(url, dataType, type, params, callbackAjax)
{
    $.ajax({
        url: url,
        dataType: dataType,
        type: type,
        data: params,
        success: function (data) {
            callbackAjax(data);
        }
    });
}

/**
 * Valida se o e-mail informado está no padrão correto.
 *
 * @param email
 * @returns {boolean}
 */
function validateEmail(email)
{
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/**
 * Função responsável por habilitar o botão para ir para o próximo passo.
 *
 * @param idList
 * @param lengthList
 */
function nextStep(idList, lengthList)
{
    var i = 0;
    for (ind in idList) {
        if ($(idList[ind]).val() !== '') {
            i++;
        }
    }

    if (i == lengthList) {
        $('.next-step').prop("disabled", false);
    } else {
        $('.next-step').prop("disabled", true);
    }

    return;
}

/**
 * Retorna um JSON no formato string formatado para um Option.
 * @param data
 * @returns {string}
 */
function options(data)
{
    data = JSON.parse(data);
    var htmlOption = '';
    for (i in data) {
        htmlOption += '<option value="'+data[i].id+'">' + data[i].name + '</option>';
    }

    return htmlOption;
}
