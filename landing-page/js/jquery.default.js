$(function() {

    // Máscaras previamente configuradas
    $('#data-nascimento').mask('99/99/9999');

    $('.next-step').prop("disabled", true);

    // Validação do nome
    $('#nome').blur(function(){
        if ($(this).val().split(' ').length > 1) {
            $(this).parent().removeClass('has-error').addClass('has-success');
            $('#help-nome').hide();
        } else {
            $(this.focus());
            $(this).parent().removeClass('has-success').addClass('has-error');
            $('#help-nome').show();
            $('.next-step').addClass('disabled');
        }

        nextStep(['#nome', '#data-nascimento', '#email', '#telefone'], 4);
    });

    // Data de nascimento, só validação se foi devidamente preenchida.
    $('#data-nascimento').blur(function() {
        nextStep(['#nome', '#data-nascimento', '#email', '#telefone'], 4);
    });

    // Valida o email informado
    $('#email').blur(function() {
        if (validateEmail($(this).val())) {
            $(this).parent().removeClass('has-error').addClass('has-success');
            $('#help-email').hide();
        } else {
            $(this.focus());
            $(this).parent().removeClass('has-success').addClass('has-error');
            $('#help-email').show();
            $('.next-step').addClass('disabled');
        }

        nextStep(['#nome', '#data-nascimento', '#email', '#telefone'], 4);
    });

    // Valido o telefone
    $('#telefone').blur(function() {
        nextStep(['#nome', '#data-nascimento', '#email', '#telefone'], 4);
    });

    $('#step1').click(function() {
        $.get('/regioes', function(data) {
            $('#regiao').html('<option value="">Selecione a sua região</option>');
            $('#regiao').append(options(data));
        });
    });

    $(this).on('change', '#regiao', function() {
        $.get('/unidades/' + $(this).val(), function(data) {
            $('#unidade').html('<option value="">Selecione a unidade mais próxima</option>');
            $('#unidade').append(options(data));
        });
    });

    $('#step2').on('click', function(event) {
        event.preventDefault();

        var params = {
            full_name: $('#nome').val(),
            birthday: $('#data-nascimento').val(),
            email: $('#email').val(),
            telephone: $('#telefone').val(),
            region_id: $('#regiao').val(),
            unit_id: $('#unidade').val()
        };

        $.post('/lead', params, function(data) {

        });
    });
});
