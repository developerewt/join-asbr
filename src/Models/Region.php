<?php

namespace Developerewt\Models;

use Developerewt\Core\Database;
use Carbon\Carbon;

/**
 * Model de Região
 *
 * Class Region
 * @package Developerewt\Models
 * @author Ewerton Melo <developerewt@gmail.com>
 * @license MIT
 * @version 0.0.1
 */
class Region extends Database
{
    
    public function __construct()
    {
        parent::__construct();

        $this->table = $this->db->table('regions');
    }

    /**
     * Retorna todas as regiões ativas.
     *
     * @return array|static[]
     */
    public function getRegions()
    {
        return $this->table->where('status', 1)->get();
    }

    /**
     * Retorna uma Região pelo seu ID
     *
     * @param $id
     * @return mixed|static
     */
    public function getRegion($id)
    {
        return $this->table->where('id', $id)->first();
    }

    /**
     * Faço a inserção de uma nova região.
     *
     * @param array $data
     * @return bool
     */
    public function insertRegion(array $data)
    {
        return $this->table->insert($data);
    }

    /**
     * Altera os dados de uma região.
     *
     * @param $id
     * @param array $data
     */
    public function updateRegion($id, array $data)
    {
        $data['updated_at'] = Carbon::now();
        return $this->table->where('id', $id)->update($data);
    }

    /**
     * Desativa uma região.
     *
     * @param $id
     * @return int
     */
    public function deleteRegion($id)
    {
        $data['updated_at'] = Carbon::now();
        return $this->table->where('id', $id)->update(['status' => 0]);
    }
}
