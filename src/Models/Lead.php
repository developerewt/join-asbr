<?php

namespace Developerewt\Models;

use Developerewt\Core\Database;
use Carbon\Carbon;

/**
 * Model de Lead
 *
 * Class Lead
 * @package Developerewt\Models
 * @author Ewerton Melo <developerewt@gmail.com>
 * @license MIT
 * @version 0.0.1
 */
class Lead extends Database
{
    public function __construct()
    {
        parent::__construct();

        $this->table = $this->db->table('leads');
    }

    /**
     * Retorna todas os leads ativas.
     *
     * @return array|static[]
     */
    public function getLeads()
    {
        return $this->table->where('status', 1)->get();
    }

    /**
     * Retorna um lead pelo seu ID
     *
     * @param $id
     * @return mixed|static
     */
    public function getLead($id)
    {
        return $this->table->where('id', $id)->first();
    }

    /**
     * Faço a inserção de um novo lead.
     *
     * @param array $data
     * @return bool
     */
    public function insertLead(array $data)
    {
        $data['created_at'] = Carbon::now();
        return $this->table->insert($data);
    }

    /**
     * Altera os dados de um lead.
     *
     * @param $id
     * @param array $data
     */
    public function updateLead($id, array $data)
    {
        $data['updated_at'] = Carbon::now();
        return $this->table->where('id', $id)->update($data);
    }

    /**
     * Desativa um lead.
     *
     * @param $id
     * @return int
     */
    public function deleteLead($id)
    {
        $data['updated_at'] = Carbon::now();
        return $this->table->where('id', $id)->update(['status' => 0]);
    }

}
