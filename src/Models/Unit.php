<?php
/**
 * Created by PhpStorm.
 * User: ewertonmelo
 * Date: 01/08/16
 * Time: 22:55
 */

namespace Developerewt\Models;

use Developerewt\Core\Database;
use Carbon\Carbon;

/**
 * Model de Unidade
 *
 * Class Unit
 * @package Developerewt\Models
 * @author Ewerton Melo <developerewt@gmail.com>
 * @license MIT
 * @version 0.0.1
 */
class Unit extends Database
{
    public function __construct()
    {
        parent::__construct();

        $this->table = $this->db->table('units');
    }

    /**
     * Retorna todas as regiões ativas.
     *
     * @return array|static[]
     */
    public function getUnits()
    {
        return $this->table->where('status', 1)->get();
    }

    /**
     * Retorna as unidades a partir de uma região.
     *
     * @param $id
     * @return array|static[]
     */
    public function getUnitsByRegion($id)
    {
        return $this->table->where('region_id', $id)
                           ->where('status', 1)
                           ->get();
    }

    /**
     * Retorna uma Região pelo seu ID
     *
     * @param $id
     * @return mixed|static
     */
    public function getUnit($id)
    {
        return $this->table->where('id', $id)->first();
    }

    /**
     * Faço a inserção de uma nova região.
     *
     * @param array $data
     * @return bool
     */
    public function insertUnit(array $data)
    {
        return $this->table->insert($data);
    }

    /**
     * Altera os dados de uma região.
     *
     * @param $id
     * @param array $data
     */
    public function updateUnit($id, array $data)
    {
        $data['updated_at'] = Carbon::now();
        return $this->table->where('id', $id)->update($data);
    }

    /**
     * Desativa uma região.
     *
     * @param $id
     * @return int
     */
    public function deleteUnit($id)
    {
        $data['updated_at'] = Carbon::now();
        return $this->table->where('id', $id)->update(['status' => 0]);
    }
}
