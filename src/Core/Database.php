<?php

namespace Developerewt\Core;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

/**
 * Carrega a conexão com o banco de dados.
 *
 * Class Database
 * @package Developerewt\Core
 * @author Ewerton Melo <developerewt@gmail.com>
 * @license MIT
 * @version 0.0.1
 */
class Database
{

    protected $db;

    /**
     * Nesse construtor eu tenho a conexão com o banco de dados.
     * Observe que a conexão é feita com os parâmetros adquiridos
     * através do arquivo .env na raiz do projeto. Bem como eu
     * decidi usar um pacote ORM para essa finalidade e a bola da
     * vez foi o Eloquent do Laravel, é bem simples de usar e de
     * instalar, minha ideia não é aumentar muito a complexidade
     * do código.
     *
     * Database constructor.
     */
    public function __construct()
    {
        $this->db = new DB;

        $this->db->addConnection([
            'driver'    => getenv('DB_DRIVER'),
            'host'      => getenv('DB_HOST'),
            'database'  => getenv('DB_DATABASE'),
            'username'  => getenv('DB_USER'),
            'password'  => getenv('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        $this->db->setEventDispatcher(new Dispatcher(new Container));

        $this->db->setAsGlobal();

        $this->db->bootEloquent();
    }
}
