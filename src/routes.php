<?php

/**
 * Script contendo as "rotas" que podem ser carregadas
 * conforme a requisição do usuário.
 *
 * @author Ewerton Melo <developerewt@gmail.com>
 * @license MIT
 * @version 0.0.1
 */

use Developerewt\Controllers\DefaultController;
use Respect\Rest\Router;

$r = new Router;
$controller = new DefaultController();

$r->any('/', function() use($controller) {
    $controller->index();
});

$r->any('/regioes', function() use($controller) {
    $controller->regioes();
});

$r->any('/unidades/*', function($id) use($controller) {
    $controller->unidades($id);
});

$r->any('/lead', function() use($controller) {
    $controller->lead();
});
