<?php

namespace Developerewt\Controllers;

use Developerewt\Models\Region;
use Developerewt\Models\Unit;
use Developerewt\Models\Lead;
use GuzzleHttp\Client;
use Zend\Json\Json;

/**
 * Controller padrão da aplicação, responsável
 * por carregar todas as requisições.
 *
 * Class DefaultController
 * @package Developerewt\Controllers
 * @author Ewerton Melo <developerewt@gmail.com>
 * @license MIT
 * @version 0.0.1
 */
class DefaultController
{
    /**
     * Objeto para receber o Model Region.
     * @var Region
     */
    private $region;

    /**
     * Objeto para receber o Model Unit.
     * @var Unit
     */
    private $unit;

    /**
     * Objeto para receber o Model Lead.
     * @var Lead
     */
    private $lead;

    /**
     * Objeto para receber a instância da lib GuzzleHttp.
     * @var Client
     */
    private $clientHttp;

    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->region = new Region();
        $this->unit = new Unit();
        $this->lead = new Lead();
        $this->clientHttp = new Client();
    }

    /**
     * Ação responsável por carregar a tela inicial.
     */
    public function index()
    {
        include_once __DIR__ . '/../../landing-page/index.phtml';
    }

    /**
     * Ação responsável por carregar as regiões ativas.
     */
    public function regioes()
    {
        echo Json::encode($this->region->getRegions());
    }

    /**
     * Ação responsável por carregar as unidades
     * ativas correspondentes a uma Região.
     *
     * @param $id
     */
    public function unidades($id)
    {
        echo Json::encode($this->unit->getUnitsByRegion($id));
    }

    /**
     * Controller responsável por salvar o lead e enviar para
     * o WebService da ActualSales.
     */
    public function lead()
    {
        if (!$_POST) {
            echo Json::encode(['error' => 'O formulário não foi submetido corretamente.']);
            die();
        }

        $data = $_POST;

        $regionSelected = $this->region->getRegion($data['region_id']);
        $unitSelected   = $this->unit->getUnit($data['unit_id']);

        $data['score'] = array_sum([10, $regionSelected->score, scoreAge($data['birthday'])]);
        $data['birthday'] = dateBD($data['birthday']);
        $data['token'] = getenv('ASBR_TOKEN');

        $this->lead->insertLead($data);
        $this->clientHttp->request('POST', 'http://api.actualsales.com.br/join-asbr/ti/lead', [
                'form_params' => [
                    'nome' => $data['full_name'], 'email' => $data['email'],
                    'telefone' => $data['telephone'], 'regiao' => $regionSelected->name,
                    'unidade' => $unitSelected->name, 'data_nascimento' => $data['birthday'],
                    'score' => $data['score'], 'token' => getenv('ASBR_TOKEN')
                ]
            ]
        );
    }

}
