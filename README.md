# Teste: Ewerton Melo de Souza

Quanto ao meu teste, pensei por muito tempo sobre como executar o que foi pedido, pensei em usar um framework, mas acho que vocês não querem ficar olhando centenas de arquivos sendo que no final o importante é uns 4 ou 5 arquivos, então, vou fazer algo bem simples usando apenas o que a linguagem PHP tem de melhor.

## Sobre a estrutura

Será algo bem simples. 

**No backend:**
- Vou carregar algumas informações usando `DotEnv` a partir do arquivo `.env` na raiz do projeto; 
- Vou configurar o `Eloquent ORM` para acessarmos o banco de dados - por quê a vida é muito curta para escrever SQL na mão;
- Manipularemos data/hora através do `Carbon`;
- Manipularemos JSON com `zend-json`;
- Vou usar o `Symfony Var-Dumper` para me auxiliar no debug;


**No front-end:**
Não precisará fazer nada. Tudo o que fiz foi com Javascript e o bom e velho JQuery, até pensei em usar algo compilado mas não seria interessante agora, se eventualmente eu tiver mais tempo para fazer maiores adições, com certeza faço algumas brincadeiras com AngularJS2 no Front-end!!! ;)

O meu desejo é que você descompacte esse pacote, execute o arquivo `dump.sql` que está na raiz desse projeto, execute `composer install` e pronto, veja tudo funcionando. Claro, não esqueça de colocar as informações de conexão com o seu banco de dados no arquivo `.env` na raiz do projeto!!!

### Melhorias

Existem algumas coisas que me incomodam bastante e, no meu código era a questão do roteamento das páginas, então, cheguei em casa e decidi incluir um roteamento decente através de um Controller. Outra mudança importante é que a partir de agora as URLs são amigáveis. Também exclui aquela pasta `/api` com aquele código em formato de script, estava muito feio.

**No backend**
- Criação de rotas e carregamento de um Controller com `Respect\Rest`;

## Sobre mim

Vou confessar, meu [Github](https://github.com/developerewt) é uma horrível, meu tempo não é o meu melhor amigo. Mas, acredito que seja mais interessante dar uma olhada nos [meus gists](https://gist.github.com/developerewt), lá com certeza tem mais conteúdo.
