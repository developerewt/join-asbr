<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Vamos carregar as nossas configurações de forma limpa e clara
 * usando o DotEnv, é muito simples de usar, olha só, com 3 linhas
 * de código já tenho tudo o que preciso configurado.
 */
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

/**
 * É feito o carregamento das funções básicas da aplicação.
 */
include_once __DIR__ . '/helpers/functions.php';

/**
 * É feito o carregamento das rotas da aplicação.
 */
include_once __DIR__ . '/src/routes.php';
