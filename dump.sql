CREATE SCHEMA asbr DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE asbr.regions (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) NOT NULL,
    score INT,
    status TINYINT,
    created_at DATETIME,
    updated_at DATETIME,
    INDEX ind_name (name)
) ENGINE=InnoDB;

CREATE TABLE asbr.units(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	region_id INT NOT NULL,
    name VARCHAR(60) NOT NULL,
    status TINYINT,
    created_at DATETIME,
    updated_at DATETIME,
    INDEX ind_name (name),
    FOREIGN KEY (region_id) REFERENCES asbr.regions(id)
) ENGINE=InnoDB;

CREATE TABLE asbr.leads (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(60) NOT NULL,
    email VARCHAR(60) NOT NULL,
    telephone VARCHAR(20),
    region_id INT NOT NULL,
    unit_id INT NOT NULL,
    birthday DATE,
    score TINYINT NOT NULL,
    token VARCHAR(255),
    created_at DATETIME,
    updated_at DATETIME,
    INDEX ind_nome (full_name),
    INDEX ind_email (email),
    FOREIGN KEY (region_id) REFERENCES asbr.regions(id),
    FOREIGN KEY (unit_id) REFERENCES asbr.units(id)
) ENGINE=InnoDB;

INSERT INTO regions(name, score, status, created_at) VALUES
("Norte", -5, 1, NOW()),
("Nordeste", -4, 1, NOW()),
("Sul", -2, 1, NOW()),
("Sudeste", -1, 1, NOW()),
("Centro-Oeste", -3, 1, NOW());

INSERT INTO units(region_id, name, status, created_at) VALUES
(1, "INDISPONÍVEL", 1, NOW()),
(2, "Salvador", 1, NOW()), (2, "Recife", 1, NOW()),
(3, "Porto Alegre", 1, NOW()), (3, "Curitiba", 1, NOW()),
(4, "São Paulo", 1, NOW()), (4, "Rio de Janeiro", 1, NOW()), (4, "Belo Horizonte", 1, NOW()),
(5, "Brasília", 1, NOW());


