<?php

/**
 * Retorna o score por idade.
 *
 * @param $birthday
 * @return int
 */
function scoreAge($birthday)
{
    $b = Carbon\Carbon::createFromFormat('d/m/Y', $birthday);
    if ($b->age >= 18 && $b->age <= 39) {
        return 0;
    } elseif ($b->age >= 40 && $b->age <= 99) {
        return -3;
    } else {
        return -5;
    }
}

/**
 * Retorna a data no padrão do banco de dados
 *
 * @param $birthday
 * @return string
 */
function dateBD($birthday)
{
    $b = Carbon\Carbon::createFromFormat('d/m/Y', $birthday);
    return $b->format('Y-m-d');
}
